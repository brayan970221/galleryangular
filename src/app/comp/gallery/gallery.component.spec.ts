import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { Picture } from 'src/app/models/picture';
import { Response } from 'src/app/models/response';
import { GalleryComponent } from './gallery.component';

describe('GalleryComponent', () => {
  let component: GalleryComponent;
  let fixture: ComponentFixture<GalleryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {
            path: 'Gallery',
            component: GalleryComponent,
          },
        ]),
        HttpClientTestingModule,
        FormsModule,
        ReactiveFormsModule,
      ],
      declarations: [ GalleryComponent ],
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GalleryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    spyOn(window, 'confirm').and.returnValue(true);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ReadAllPictures test', () => {
    const res: Picture[] = [];
    spyOn(component.pictureService, 'readAllPictures').and.returnValue(of(res));
    component.readAllPictures();
    expect(component.error).toBeFalsy();
    expect(component.pictures).toEqual(res);
  });

  it('DeletePicture test', () => {
    const res: Response = { msg: 'Deleted successfully' };
    spyOn(component.pictureService, 'deletePicture').and.returnValue(of(res));
    component.deletePicture('1');
    expect(component.error).toBeFalsy();
    expect(component.deleteResponse).toEqual(res);
  });

  it('CleanForm test', () => {
    component.picForm.controls['id'].setValue('50');
    component.picForm.controls['title'].setValue('3');
    component.picForm.controls['numberQualifications'].setValue(2);
    component.picForm.controls['totalQualifications'].setValue(8);
    component.cardImageBase64 = 'T E S T';
    component.isImageSaved = true;
    component.cleanForm();
    expect(component.picForm.controls['id'].value).toEqual('');
    expect(component.picForm.controls['title'].value).toEqual('');
    expect(component.picForm.controls['numberQualifications'].value).toEqual('');
    expect(component.picForm.controls['totalQualifications'].value).toEqual('');
    expect(component.picForm.controls['image'].value).toEqual('');
    expect(component.cardImageBase64).toEqual('');
    expect(component.isImageSaved).toBeFalsy();
  });

  it('creation success', () => {
    const res: Response = { msg: 'Created successfully' };
    spyOn(component.pictureService, 'createPicture').and.returnValue(of(res));
    component.createPicture();
    expect(component.responseCU).toEqual(res);
  });
  it('creation failed', () => {
    const res: Response = { msg: 'Another msg' };
    spyOn(component.pictureService, 'createPicture').and.returnValue(of(res));
    spyOn(window, 'alert');
    component.createPicture();
    expect(window.alert).toHaveBeenCalledWith(res.msg);
  });

  it('Valid form', () => {
    component.picForm.controls['id'].setValue('50');
    component.picForm.controls['title'].setValue('Test');
    component.picForm.controls['numberQualifications'].setValue(2);
    component.picForm.controls['totalQualifications'].setValue(8);
    component.picForm.controls['image'].setValue(123);
    expect(component.picForm.valid).toBeTruthy();
    component.onSubmit();
    expect(component.submitFlag).toBeFalsy();
  });

  it('Invalid form', () => {
    component.picForm.controls['id'].setValue('50');
    component.picForm.controls['title'].setValue('3');
    component.picForm.controls['numberQualifications'].setValue(2);
    component.picForm.controls['totalQualifications'].setValue(8);
    component.onSubmit();
    expect(component.picForm.valid).toBeFalsy();
    expect(component.submitFlag).toBeTruthy();
  });


  it('Input Change', () => {
    const input  = fixture.debugElement.query(By.css('input[type=file]')).nativeElement;
    spyOn(component, 'fileChangeEvent');
    input.dispatchEvent(new Event('change'));
    expect(component.fileChangeEvent).toHaveBeenCalled();
  });

  it('Round Prom', () => {
    const input  = component.round(20.89);
    expect(input).toEqual(21);
  });
});
