/* eslint-disable @typescript-eslint/explicit-module-boundary-types */
/* eslint-disable @typescript-eslint/no-explicit-any */
import { Component,  OnInit, ViewChild} from '@angular/core';
import { PictureService } from 'src/app/services/picture.service';
import { Picture } from 'src/app/models/picture';
import { Response } from 'src/app/models/response';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css'],
})

export class GalleryComponent implements OnInit {
  @ViewChild('myFile')
  myInputVariable: any;
  editFlag = false;
  submitFlag = false;
  imageError = '';
  isImageSaved = false;
  cardImageBase64 = '';
  pic: Picture = {
    id: '0',
    numberQualifications: 0,
    totalQualifications: 0,
  };
  responseCU: Response = {};
  pictures: Picture[] = [];
  error = false;
  deleteResponse: Response = {};

  constructor(public pictureService: PictureService,
    private fb: FormBuilder,) {}

  ngOnInit(): void {
    this.readAllPictures();
  }

  picForm = this.fb.group({
    id: ['', [Validators.required, Validators.min(1)]],
    title: ['', Validators.required],
    numberQualifications: ['', [Validators.required, Validators.min(1)]],
    totalQualifications: ['', [Validators.required, Validators.min(1)]],
    image: ['', Validators.required],
  });

  /**
   * Use the pictureService for read all the pictures an asign the response to
   * local pictures array if the request is correct
   */
  readAllPictures(): void {
    this.pictureService.readAllPictures().subscribe((res) => {
      this.pictures = res as Picture[];
      this.error = false;
    });
  }

  /**
   * Verify the form and edit or create a picture
   */
  onSubmit(): void {
    if (this.picForm.valid) {
      this.submitFlag = false;
      this.pic.id = this.picForm.controls['id'].value;
      this.pic.numberQualifications = this.picForm.controls[
        'numberQualifications'
      ].value;
      this.pic.totalQualifications = this.picForm.controls[
        'totalQualifications'
      ].value;
      this.pic.image = this.cardImageBase64;
      this.pic.title = this.picForm.controls['title'].value;
      this.createPicture();
      this.cleanForm();
    } else {
      this.submitFlag = true;
    }
  }

  round(n: number): number {
    return Math.round(n);
  }

  fileChangeEvent(fileInput: any): void {
    this.imageError = '';
    if (fileInput.target.files && fileInput.target.files[0]) {
      // Size Filter Bytes
      const max_size = 20971520;

      if (fileInput.target.files[0].size > max_size) {
        this.imageError = 'Maximum size allowed is ' + max_size / 1000 + 'Mb';
      }

      const reader = new FileReader();
      reader.onload = (e: any) => {
        const image = new Image();
        image.src = e.target.result;
        const imgBase64Path = e.target.result;
        this.cardImageBase64 = imgBase64Path;
        this.isImageSaved = true;
        this.picForm.controls["image"].setValue(imgBase64Path);
      };
      reader.readAsDataURL(fileInput.target.files[0]);
    }
  }

  createPicture(): void {
    this.pictureService.createPicture(this.pic).subscribe((res) => {
      this.responseCU = res as Response;
      if (this.responseCU.msg == 'Saved successfully') {
        this.readAllPictures();
      } else {
        alert(this.responseCU.msg);
      }
    });
  }

  cleanForm(): void {
      this.picForm.controls['id'].setValue('');
      this.picForm.controls['title'].setValue('');
      this.picForm.controls['numberQualifications'].setValue('');
      this.picForm.controls['totalQualifications'].setValue('');
      this.picForm.controls['image'].setValue('');
      this.cardImageBase64 = '';
      this.isImageSaved = false;
      this.myInputVariable.nativeElement.value = "";
  }

  /**
   * Use the pictureService for delete a picture using the id
   *
   * @param id String that correspond with the id of the picture
   */
  deletePicture(id: string): void {
    if (confirm('Are you sure you want to delete this picture?')) {
      this.pictureService.deletePicture(id).subscribe((res) => {
        this.readAllPictures();
        this.deleteResponse = res as Response;
      });
    }
  }
}
