export interface Picture {
  id: string;
  numberQualifications: number;
  totalQualifications: number;
  title?: string;
  image?: string;
}
