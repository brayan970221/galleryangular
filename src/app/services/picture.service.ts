import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Picture } from '../models/picture';
import { Response } from 'src/app/models/response';

@Injectable({
  providedIn: 'root'
})
export class PictureService {
  API_URI = 'http://localhost:8081/Picture';

  constructor(private http: HttpClient) {}

  /**
   * Read all pictures in the database
   *
   * @returns Json with all the pictures in the database
   */
  readAllPictures(): Observable<Picture[]> {
    return this.http.get<Picture[]>(`${this.API_URI}/read`);
  }

  /**
   * Read a picture in the database by the id
   *
   * @returns Json with the picture
   */
  readPictureById(id: string): Observable<Picture> {
    return this.http.get<Picture>(`${this.API_URI}/read/${id}`);
  }

  /**
   * Create a picture in the database
   *
   * @param picture picture object that will be created in the database
   * @returns Message with the response for the create request
   */
  createPicture(picture: Picture): Observable<Response> {
    return this.http.post<Response>(`${this.API_URI}/create`, picture);
  }

  /**
   * Delete a picture in the databasae
   *
   * @param id Number with the id of the picture in the database
   * @returns Message with the response for the delete request
   */
  deletePicture(id: string): Observable<Response> {
    return this.http.delete<Response>(`${this.API_URI}/delete/${id}`);
  }

  /**
   * Edit a picture in the database
   *
   * @param picture Picture object that will be edited in the database
   * @returns Message with the response for the edit request
   */
  editPicture(picture: Picture): Observable<Response> {
    return this.http.put<Response>(`${this.API_URI}/edit`, picture);
  }
}
