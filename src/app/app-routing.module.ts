import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/gallery',
    pathMatch: 'full',
  },
  {
    path: 'gallery',
    loadChildren: () => import('./comp/gallery/gallery.module').then(m => m.GalleryModule),
  },
  {
    path: 'contact',
    loadChildren: () => import('./comp/contact/contact.module').then(m => m.ContactModule),
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
